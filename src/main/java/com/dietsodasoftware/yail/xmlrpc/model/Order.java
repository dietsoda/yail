package com.dietsodasoftware.yail.xmlrpc.model;

import com.dietsodasoftware.yail.xmlrpc.client.annotations.TableName;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * http://help.infusionsoft.com/developers/tables/job
 *
 * Created with IntelliJ IDEA.
 * User: wendelschultz
 * Date: 3/12/13
 * Time: 6:01 PM
 * To change this template use File | Settings | File Templates.
 */
@TableName(table = "Job")
public class Order extends Model {


    public Order(Map<String, Object> model) {
        super(model);
    }

    @Override
    public Collection<? extends NamedField> allFields(){
        return Collections.unmodifiableCollection(Arrays.asList(Field.values()));
    }

    public enum Field implements NamedField {
        Id(Integer.class, Access.Read),
        JobTitle(String.class, Access.Update, Access.Add, Access.Read),
        ContactId(Integer.class, Access.Update, Access.Add, Access.Read),
        StartDate(Date.class, Access.Update, Access.Add, Access.Read),
        DueDate(Date.class, Access.Update, Access.Add, Access.Read),
        JobNotes(String.class, Access.Update, Access.Add, Access.Read),
        ProductId(Integer.class, Access.Update, Access.Add, Access.Read),
        JobStatus(String.class, Access.Update, Access.Add, Access.Read),
        DateCreated(Date.class, Access.Read),
        JobRecurringId(Integer.class, Access.Read),
        OrderType(String.class, Access.Update, Access.Add, Access.Read),
        OrderStatus(Integer.class, Access.Update, Access.Add, Access.Read),
        ShipFirstName(String.class, Access.Update, Access.Add, Access.Read),
        ShipMiddleName(String.class, Access.Update, Access.Add, Access.Read),
        ShipLastName(String.class, Access.Update, Access.Add, Access.Read),
        ShipCompany(String.class, Access.Update, Access.Add, Access.Read),
        ShipPhone(String.class, Access.Update, Access.Add, Access.Read),
        ShipStreet1(String.class, Access.Update, Access.Add, Access.Read),
        ShipStreet2(String.class, Access.Update, Access.Add, Access.Read),
        ShipCity(String.class, Access.Update, Access.Add, Access.Read),
        ShipState(String.class, Access.Update, Access.Add, Access.Read),
        ShipZip(String.class, Access.Update, Access.Add, Access.Read),
        ShipCountry(String.class, Access.Update, Access.Add, Access.Read)
        ;

        private final Class<?> fieldClass;
        private final List<Access> fieldAccess;

        private Field(Class<?> fieldClass, Access... fieldAccess) {
            if(fieldAccess == null || fieldAccess.length == 0){ throw new RuntimeException("Invalid null fieldAccess argument"); }
            this.fieldClass = fieldClass;
            this.fieldAccess = Arrays.asList(fieldAccess);
        }

        @Override
        public Class<?> typeClass() {
            return fieldClass;
        }

        @Override
        public boolean hasAccess(Access access){
            return fieldAccess.contains(access);
        }

        @Override
        public Collection<Access> getAccess(){
            return Collections.unmodifiableList(fieldAccess);
        }
    }

    public enum OrderStatus {
      InFulfillment(0, "Customer has paid some or all of the money"),
      PendingPayment(1, "The customer has not paid yet");

      private final int dbValue;
      private final String description; // this is for self-documenting reasons only

      OrderStatus(int dbValue, String description) {
        this.dbValue = dbValue;
        this.description = description;
      }

      public int getDbValue() {
        return dbValue;
      }
    }

    public enum OrderType {
      Online("Online"),
      Offline("Offline");

      private final String dbValue;

      private OrderType(String dbValue) {
        this.dbValue = dbValue;
      }

      public String getDbValue() {
        return dbValue;
      }
    }

    public static Builder builder(){
        return new Builder();
    }

    public static Builder builder(Order prototype) {
      return new Builder(prototype);
    }

    public static class Builder extends Model.Builder<Order> {
      public Builder(Map<String, Object> values) {
        super(Order.class, values);
      }

      public Builder() {
        super(Order.class);
      }

      public Builder(Order prototype) {
        super(prototype);
      }

      public Builder setOrderType(OrderType orderType) {
        setFieldValue(Field.OrderType, orderType.dbValue);
        return this;
      }

      public Builder setOrderStatus(OrderStatus status) {
        setFieldValue(Field.OrderStatus, status.dbValue);
        return this;
      }

      public Builder setProductId(Integer id) {
        setFieldValue(Field.ProductId, id);
        return this;
      }

      public Builder setJobTitle(String title) {
        setFieldValue(Field.JobTitle, title);
        return this;
      }

      public Builder setJobStatus(String status) {
        setFieldValue(Field.JobStatus, status);
        return this;
      }

      public Builder setShipFirstName(String name) {
        setFieldValue(Field.ShipFirstName, name);
        return this;
      }

      public Builder setShipLastName(String name) {
        setFieldValue(Field.ShipLastName, name);
        return this;
      }

      public Builder setShipStreet1(String line1) {
        setFieldValue(Field.ShipStreet1, line1);
        return this;
      }

      public Builder setShipStreet2(String line2) {
        setFieldValue(Field.ShipStreet2, line2);
        return this;
      }

      public Builder setShipCity(String city) {
        setFieldValue(Field.ShipCity, city);
        return this;
      }

      public Builder setShipState(String state) {
        setFieldValue(Field.ShipState, state);
        return this;
      }

      public Builder setShipZip(String zip) {
        setFieldValue(Field.ShipZip, zip);
        return this;
      }

      public Builder setShipCountry(String country) {
        setFieldValue(Field.ShipCountry, country);
        return this;
      }
    }
}
