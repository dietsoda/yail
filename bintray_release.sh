#!/bin/bash

# Inspired by:
#  * https://axelfontaine.com/blog/final-nail.html
#  * https://axelfontaine.com/blog/dead-burried.html
#  * https://docs.gitlab.com/ce/ci/variables/README.html#predefined-variables-environment-variables

REVISION_ARG=$1
PROJECT_BRANCH_ARG=$2

if [ -z "$PROJECT_BRANCH_ARG" ]
then
  PROJECT_BRANCH_ARG=`git branch|grep "\*"|cut -d ' ' -f 2|tr -s "[:blank:]" " "`
fi

if [ -z "$GITLAB_CI" ]
then
  # running locally... ugh
  REVISION=$REVISION_ARG
  PROJECT_BRANCH=$PROJECT_BRANCH_ARG
  MVN_CLI_OPTS=''
fi


if [ -z "$PROJECT_BRANCH" ]
then
  echo "Must provide branch as second argument or run in GitLab CI (env var: 'CI_COMMIT_REF_NAME': $CI_COMMIT_REF_NAME)"
  echo "  ex: $0 <revision> <branch>    from local CLI"
  exit 1
fi

if [ -z "$REVISION" ]
then
  echo "Must provide revision number as first argument or run in GitLab CI (env var: 'CI_PIPELINE_IID': $CI_PIPELINE_IID)"
  echo "  ex: $0 <revision> <branch>    from local CLI"
  exit 1
fi

echo "Revision: '$REVISION' from ref $PROJECT_BRANCH"

echo "Maven version"
mvn -version

echo "Resolve dependencies"
mvn $MVN_CLI_OPTS dependency:tree

echo "Build and publish release version"
mvn $MVN_CLI_OPTS clean deploy scm:tag -Dbuild.revision=$REVISION

if [ $? -eq 0 ]; then
    echo Success.
else
    echo Failure building, testing, publishing, tagging or pushing tag
    exit 1
fi


echo "Done-ski™. TM™."
